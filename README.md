# Exynos 8890 Development - Local Manifest

You have found our local manifest file and you are wondering how do I use it in creating custom ROMs?

## Getting Started

These instructions will allow you get you a copy of our sources up and running on your local machine for development and testing purposes.

### Prerequisites

You must be running a Linux system and have already have established a working build environment for compiling custom ROMS and have already ran the **repo init** command in your **working directory.** *(e.g LineageOS)*

#### How to copy the local manifests

 

 - **cd** WORKING_DIRECTORY *(e.g LineageOS) if you are not there already*
  - **cd** .repo
   - **mkdir** local_manifests
   - **cd** local_manifests
   - **wget** https://gitlab.com/Exynos8890/local_manifests/raw/los/roomservice.xml
   - And then head back to your working directory and run the recommended
   repo sync command that has been suggested by the ROM developers.

#### Now you have sucessfully copied the local manifest file to your machine!